   //Time
   $('#time-karavan').timepicker({
       timeFormat: 'H:i',
       minTime: '10:00',
       maxTime: '20:00',

   });
   $('#time-karavan-1').timepicker({
       'timeFormat': 'H:i',
       minTime: '10',
       maxTime: '22',
   });
   $('#time-kubometr').timepicker({
       'timeFormat': 'H:i',
       minTime: '12',
       maxTime: '20'
   });
   $('#time-kubometr-1').timepicker({
       'timeFormat': 'H:i',
       minTime: '12',
       maxTime: '22'
   });
   $('#time-time').timepicker({
       'timeFormat': 'H:i',
       minTime: '10',
       maxTime: '20',
       step: 10,
   });
   $('#time-time-1').timepicker({
       'timeFormat': 'H:i',
       minTime: '10',
       maxTime: '22',
       step: 10,
   });


   $('#date-karavan').datepicker({
       dateFormat: 'dd/mm/yy',
   });
   $('#date-kubometr').datepicker({
       dateFormat: 'dd/mm/yy',
   });
   $('#date-time').datepicker({
       dateFormat: 'dd/mm/yy',
   });

   jQuery(function($) {
       $.datepicker.regional['ru'] = {
           closeText: 'Закрыть',
           prevText: '&#x3c;Пред',
           nextText: 'След&#x3e;',
           currentText: 'Сегодня',
           monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
               'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
           ],
           monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
               'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
           ],
           dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
           dayNamesShort: ['вск', 'пн', 'вт', 'срд', 'чт', 'пт', 'сб'],
           dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
           weekHeader: 'Нед',
           dateFormat: 'dd.mm.yy',
           firstDay: 1,
           isRTL: false,
           showMonthAfterYear: false,
           yearSuffix: ''
       };
       $.datepicker.setDefaults($.datepicker.regional['ru']);
   });

   $("#spinner-karavan").spinner({
       min: 1,
       max: 6
   });
   $("#spinner-kubometr").spinner({
       min: 1,
       max: 6
   });
   $("#spinner-time").spinner({
       min: 1,
       max: 6
   });