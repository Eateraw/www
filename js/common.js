$(document).ready(function () {
    var options = {
        offset: 50
    }
    var header = new Headhesive('header', options);
    $(function () {
        $('.wrapper-rent').click(function () {
            $(this).hide("drop", {
                direction: "down",
            }, 300);
        })
    });



    /*      $(function() {
              $('.thumb-rent').mouseleave(function() {
                      $('.wrapper-rent').show("drop", {
                          direction: "down",
                      }, 300);
                  })
              })
          });*/

    $(function () {
        $('#close-rent').click(function () {
            if (width >= 840) {
                $('.wrapper-rent').removeClass("hidden")
            }
        })
    })

    $(function () {
        $('#close-rent').bind('click.smoothscroll', function () {
            var target = $(this),
                to_top = $(".services").offset().top - 102;
            $('body, html').animate({
                scrollTop: to_top
            }, .1);
        })
    })

    $(window).scroll(function () {
        if ($(document).scrollTop() > 100) {
            $('#close-rent').removeClass("hidden")
        } else
            $('#close-rent').addClass("hidden")
    })


    $('.image').magnificPopup({
        type: 'image',
        removalDelay: 300,
        mainClass: "mfp-with-zoom",
        titleSrc: "title",
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: "ease-in-out",
            opener: function (openerElement) {
                return openerElement.is("img") ? openerElement : openerElement.find("img");
            }
        }
    });

    //E-mail Ajax Send
    $("#form").submit(function () {
        var th = $(this);
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: th.serialize()
        }).done(function () {
            alert("Thank you!");
            setTimeout(function () {
                th.trigger("reset");
            }, 1000);
        });
        return false;
    });

    $(".popup").magnificPopup();

})
