;
(function () {

    'use strict';

    var isiPad = function () {
        return (navigator.platform.indexOf("iPad") != -1);
    };

    var isiPhone = function () {
        return (
            (navigator.platform.indexOf("iPhone") != -1) ||
            (navigator.platform.indexOf("iPod") != -1)
        );
    };

    var windowHeight = ($(window).height() > 500 ? $(window).height() : 500) + 'px'

    //    function setHeiHeight() {
    //        jQuery('.overlay-services').css({
    //            height: windowHeight
    //        });
    //    };
    var mobileMenuOutsideClick = function () {

        $(document).click(function (e) {
            var container = $("#gtco-offcanvas, .js-gtco-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                $('.js-gtco-nav-toggle').addClass('gtco-nav-white');

                if ($('body').hasClass('offcanvas')) {

                    $('body').removeClass('offcanvas');
                    $('.js-gtco-nav-toggle').removeClass('active');

                }


            }
        });

    };


    var offcanvasMenu = function () {

        $('.gtco-nav').prepend('<div id="gtco-offcanvas" />');
        $('.gtco-nav').prepend('<a href="#" class="js-gtco-nav-toggle gtco-nav-toggle gtco-nav-white"><i></i></a>');
        var clone1 = $('.menu-1 > ul').clone();
        $('#gtco-offcanvas').append(clone1);
        var clone2 = $('.menu-2 > ul').clone();
        $('#gtco-offcanvas').append(clone2);

        $('#gtco-offcanvas .has-dropdown').addClass('offcanvas-has-dropdown');
        $('#gtco-offcanvas')
            .find('li')
            .removeClass('has-dropdown');

        // Hover dropdown menu on mobile
        $('.offcanvas-has-dropdown').mouseenter(function () {
            var $this = $(this);

            $this
                .addClass('active')
                .find('ul')
                .slideDown(500, 'easeOutExpo');
        }).mouseleave(function () {

            var $this = $(this);
            $this
                .removeClass('active')
                .find('ul')
                .slideUp(500, 'easeOutExpo');
        });


        $(window).resize(function () {

            if ($('body').hasClass('offcanvas')) {

                $('body').removeClass('offcanvas');
                $('.js-gtco-nav-toggle').removeClass('active');

            }
        });
    };


    var burgerMenu = function () {

        $('body').on('click', '.js-gtco-nav-toggle', function (event) {
            var $this = $(this);


            if ($('body').hasClass('overflow offcanvas')) {
                $('body').removeClass('overflow offcanvas');
            } else {
                $('body').addClass('overflow offcanvas');
            }
            $this.toggleClass('active');
            event.preventDefault();

        });
    };



    var contentWayPoint = function () {
        var i = 0;

        // $('.gtco-section').waypoint( function( direction ) {


        $('.animate-box').waypoint(function (direction) {

            if (direction === 'down' && !$(this.element).hasClass('animated-fast')) {

                i++;

                $(this.element).addClass('item-animate');
                setTimeout(function () {

                    $('body .animate-box.item-animate').each(function (k) {
                        var el = $(this);
                        setTimeout(function () {
                            var effect = el.data('animate-effect');
                            if (effect === 'fadeIn') {
                                el.addClass('fadeIn animated-fast');
                            } else if (effect === 'fadeInLeft') {
                                el.addClass('fadeInLeft animated-fast');
                            } else if (effect === 'fadeInRight') {
                                el.addClass('fadeInRight animated-fast');
                            } else {
                                el.addClass('fadeInUp animated-fast');
                            }

                            el.removeClass('item-animate');
                        }, k * 200, 'easeInOutExpo');
                    });

                }, 100);

            }

        }, {
            offset: '85%'
        });
    };


    var dropdown = function () {

        $('.has-dropdown').mouseenter(function () {

            var $this = $(this);
            $this
                .find('.dropdown')
                .css('display', 'block')
                .addClass('animated-fast fadeInUpMenu');

        }).mouseleave(function () {
            var $this = $(this);

            $this
                .find('.dropdown')
                .css('display', 'none')
                .removeClass('animated-fast fadeInUpMenu');
        });

    };


    var owlCarousel = function () {

        var owl = $('.owl-carousel-carousel');
        owl.owlCarousel({
            items: 3,
            loop: true,
            margin: 20,
            nav: true,
            dots: true,
            autoplay: true,
            smartSpeed: 1000,
            autoplayTimeout: 5000,
            navText: [
                "<i class='ti-arrow-left owl-direction'></i>",
                "<i class='ti-arrow-right owl-direction'></i>"
            ],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });


        var owl = $('.owl-carousel-fullwidth');
        owl.owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            margin: 20,
            nav: true,
            dots: true,
            smartSpeed:1000,
            autoplayTimeout:5000,
            navText: [
                "<i class='ti-arrow-left owl-direction'></i>",
                "<i class='ti-arrow-right owl-direction'></i>"
            ]
        });




    };

    var tabs = function () {

        // Auto adjust height
        $('.gtco-tab-content-wrap').css('height', 0);
        var autoHeight = function () {

            setTimeout(function () {

                var tabContentWrap = $('.gtco-tab-content-wrap'),
                    tabHeight = $('.gtco-tab-nav').outerHeight(),
                    formActiveHeight = $('.tab-content.active').outerHeight(),
                    totalHeight = parseInt(tabHeight + formActiveHeight + 90);

                tabContentWrap.css('height', totalHeight);

                $(window).resize(function () {
                    var tabContentWrap = $('.gtco-tab-content-wrap'),
                        tabHeight = $('.gtco-tab-nav').outerHeight(),
                        formActiveHeight = $('.tab-content.active').outerHeight(),
                        totalHeight = parseInt(tabHeight + formActiveHeight + 90);

                    tabContentWrap.css('height', totalHeight);
                });

            }, 100);

        };

        autoHeight();


        // Click tab menu
        $('.gtco-tab-nav a').on('click', function (event) {

            var $this = $(this),
                tab = $this.data('tab');

            $('.tab-content')
                .addClass('animated-fast fadeOutDown');

            $('.tab-content')
                .removeClass('active');

            $('.gtco-tab-nav li').removeClass('active');

            $this
                .closest('li')
                .addClass('active')

            $this
                .closest('.gtco-tabs')
                .find('.tab-content[data-tab-content="' + tab + '"]')
                .removeClass('animated-fast fadeOutDown')
                .addClass('animated-fast active fadeIn');


            autoHeight();
            event.preventDefault();

        });
    };


    var goToTop = function () {

        $('.js-gotop').on('click', function (event) {

            event.preventDefault();

            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');

            return false;
        });

        $(window).scroll(function () {

            var $win = $(window);
            if ($win.scrollTop() > 200) {
                $('.js-top').addClass('active');
            } else {
                $('.js-top').removeClass('active');
            }

        });

    };


    // Loading page
    //    var loaderPage = function () {
    //        $('#bgndVideo').on("ready",function(e){
    //        $(".gtco-loader").fadeOut("slow");
    //        });
    //    };

    var counter = function () {
        $('.js-counter').countTo({
            formatter: function (value, options) {
                return value.toFixed(options.decimals);
            },
        });
    };

    var counterWayPoint = function () {
        if ($('#gtco-counter').length > 0) {
            $('#gtco-counter').waypoint(function (direction) {

                if (direction === 'down' && !$(this.element).hasClass('animated')) {
                    setTimeout(counter, 400);
                    $(this.element).addClass('animated');
                }
            }, {
                offset: '90%'
            });
        }
    };

    //Мои доработки

    // one Page Nav
    ;
    if ($(document).ready(function () {
            $("a.sidebar1").on("click", function (e) {
                $(this).attr("href");
                $("html, body").animate({
                    scrollTop: $("#gtco-price").offset().top - 100
                }, "slow"), e.preventDefault()
            }), $("a.sidebar2").on("click", function (e) {
                $(this).attr("href");
                $("html, body").animate({
                    scrollTop: $("#gtco-features-3").offset().top - 40
                }, "slow"), e.preventDefault()
            }), $("a.sidebar3").on("click", function (e) {
                $(this).attr("href");
                $("html, body").animate({
                    scrollTop: $("#gtco-portfolio").offset().top - 25
                }, "slow"), e.preventDefault()
            }), $("a.playgame").on("click", function (e) {
                $(this).attr("href");
                $("html, body").animate({
                    scrollTop: $("#bron-vremeny").offset().top - 100
                }, "slow"), e.preventDefault()
            }), $("a#bron-vremeny-popup").on("click", function (e) {
                $(this).attr("href");
                $("html, body").animate({
                    scrollTop: $("#bron-vremeny").offset().top - 100
                }, "slow"), e.preventDefault()
            }), $("a.btn-order").on("click", function (e) {
                $(this).attr("href");
                $("html, body").animate({
                    scrollTop: $("#gtco-subscribe").offset().top - 100
                }, "slow"), e.preventDefault()
            });
        }))

        var linkS1 = function () {

            $('body').on('click', '#gtco-offcanvas .sidebar1', function (event) {
                var $this = $(this);
                $(this).attr("href");
                $("html, body").animate({
                    scrollTop: $("#gtco-price").offset().top - 100
                }, 2500), event.preventDefault()

                if ($('body').hasClass('overflow offcanvas')) {
                    $('body').removeClass('overflow offcanvas');
                } else {
                    $('body').addClass('overflow offcanvas');
                }
                $('.js-gtco-nav-toggle').removeClass('active');
                event.preventDefault();


            });
        };

    var linkS2 = function () {

        $('body').on('click', '#gtco-offcanvas .sidebar2', function (event) {
            var $this = $(this);
            $(this).attr("href");
            $("html, body").animate({
                scrollTop: $("#gtco-features-3").offset().top - 100
            }, 2500), event.preventDefault()

            if ($('body').hasClass('overflow offcanvas')) {
                $('body').removeClass('overflow offcanvas');
            } else {
                $('body').addClass('overflow offcanvas');
            }
            $('.js-gtco-nav-toggle').removeClass('active');
            event.preventDefault();


        });
    };

    var linkS3 = function () {

        $('body').on('click', '#gtco-offcanvas .sidebar3', function (event) {
            var $this = $(this);
            $(this).attr("href");
            $("html, body").animate({
                scrollTop: $("#gtco-portfolio").offset().top - 100
            }, 2500), event.preventDefault()

            if ($('body').hasClass('overflow offcanvas')) {
                $('body').removeClass('overflow offcanvas');
            } else {
                $('body').addClass('overflow offcanvas');
            }
            $('.js-gtco-nav-toggle').removeClass('active');
            event.preventDefault();


        });
    };

    window.onscroll = function showHeader() {
        var header = document.querySelector('.gtco-nav-toggle')

        if (window.pageYOffset > 50) {
            header.classList.add('gtco-nav-toggled');
        }
    }

    window.onscroll = function showHeader() {
        var header = document.querySelector('.gtco-nav')

        if (window.pageYOffset > 50) {
            header.classList.add('gtco-nav-toggled');
        } else
            header.classList.remove('gtco-nav-toggled')
    };

    //MagnificPopup
    $('.popup').magnificPopup();

    //    //Calendar
    //    $(".datepicker").attr("autocomplete", "off");
    //
    //    $(document).ready(function () {
    //
    //        //E-mail Ajax Send HAPPY_TIME
    //        $("#happy").submit(function () {
    //            var th = $(this);
    //            $.ajax({
    //                type: "POST",
    //                url: "mail.php",
    //                data: th.serialize()
    //            }).done(function () {
    //                alert("Спасибо! В ближайшее время с вами свяжутся для уточнения заказа");
    //                setTimeout(function () {
    //                    th.trigger("reset");
    //                }, 1000);
    //            });
    //            return false;
    //        });
    //    });

    $(window).on('load', (function () {
        var scriptViaronMap = document.createElement('script');
        scriptViaronMap.src = 'js/viaronMap.js';
        scriptViaronMap.type = 'text/javascript';
        setTimeout(function () {
            document.body.parentNode.appendChild(scriptViaronMap);
        }, 1000);
    }));

    var monitor = setInterval(function () {
        var elem = document.activeElement;
        if (elem && elem.tagName == 'IFRAME') {
            fbq("trackCustom", "Нажатие на форму")
            clearInterval(monitor);
        }
    }, 100);

    /* Video background */

    $(function () {
        //        setHeiHeight();
        //        $(window).resize(setHeiHeight);
        mobileMenuOutsideClick();
        offcanvasMenu();
        burgerMenu();
        linkS1();
        linkS2();
        linkS3();
        contentWayPoint();
        dropdown();
        owlCarousel();
        tabs();
        goToTop();
        //        loaderPage();
        counterWayPoint();
    });


}());
